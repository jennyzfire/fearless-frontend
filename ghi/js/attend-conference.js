window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
        console.log(data);

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      let selectLoading = document.querySelector('.spinner-grow');
      selectLoading.classList.add("d-none");
      selectTag.classList.remove("d-none");
    }

    const selectAttendeeForm = document.querySelector("#create-attendee-form");
    selectAttendeeForm.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(selectAttendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData));

        const url = "http://localhost:8001/api/attendees/";
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfig);

        if(response.ok){
            selectAttendeeForm.reset();

            let selectAlert = document.querySelector("#success-message");
            selectAlert.classList.remove("d-none")

            selectAttendeeForm.classList.add("d-none");
            const newAttendee = await response.json();
            console.log(newAttendee);
        }
    })
  });
