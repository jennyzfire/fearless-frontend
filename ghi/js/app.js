function createCard(name, description, pictureUrl, start, end, location) {
    return `
        <div class="card shadow-lg p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer">${start} - ${end}</div>
            </div>
        </div>`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        window.alert("no thanks");
      } else {
        const data = await response.json();
        let i = 0;

        for(let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            if(detailResponse.ok) {
                const details = await detailResponse.json();
                // const title = details.conference.title;
                const name = details.conference.name;
                const description = details.conference.description;
                const location = details.conference.location.name;
                const start = new Date(details.conference.starts).toLocaleDateString();
                const end = new Date(details.conference.ends).toLocaleDateString();
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(name, description, pictureUrl, start, end, location);

                const column = document.querySelectorAll(`.col`);
                if(i > 2) {
                    i = 0;
                }
                column[i].innerHTML += html;
            }
            i++;
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error('error', e)
    }

  });
