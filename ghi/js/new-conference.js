window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/"

    try {
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json() // location instances
            const selectTag = document.querySelector('#location')

            for(let location of data.locations){
                const newOption = document.createElement("option");
                newOption.value = location.id;
                newOption.innerHTML = location.name;
                selectTag.appendChild(newOption);
            }
        }
    } catch (e) {
        console.error('error', e);
    }

    const formTag = document.querySelector('#create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);  //FormData knows how to extract the data from formTag
        // formData is a new class, and somewhere inside it, it has the HTML input data.
        const json = JSON.stringify(Object.fromEntries(formData));
        // changed formData into JSON string
        const locationUrl = "http://localhost:8000/api/conferences/"; //API endpoint, destination
        const fetchConfig = { // content of what you are transporting!!!!
            method: "post",
            body: json,  //payload
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(locationUrl, fetchConfig);

        if(response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    })
})
