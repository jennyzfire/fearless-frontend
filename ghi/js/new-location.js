window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/"

    try {
        const response = await fetch(url);  // response is an object with a status code, ok, etc. because of await.

        if(response.ok) {
            const data = await response.json(); // data is an object
            const selectTag = document.querySelector('#state') // query selector works like css
            console.log(data);
            for(let stateDict of data.states){
                for(let key in stateDict){
                    const newOption = document.createElement("option");
                    newOption.value = stateDict[key];
                    newOption.innerHTML = key;
                    selectTag.appendChild(newOption);
                // create option element (option tag in html)
                // createElement(tagName, options)
                // set the option to state name
                // set the value to state abbreviation
                }
            }
        }

    } catch (e) {
        console.error('error', e);
    }
    // getting data from the location form submitted
    const formTag = document.querySelector('#create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // need to take the data and convert to JSON for API call
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = "http://localhost:8000/api/locations/";
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(locationUrl, fetchConfig);

        if(response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    })
})
