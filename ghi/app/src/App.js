import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './ConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <div>
      <Nav />
      <div className="container">
        {/*<LocationForm />*/}
        <ConferenceForm />
        {/*<AttendeesList attendees={props.attendees}/>*/}
      </div>
    </div>
  );
}

export default App;
