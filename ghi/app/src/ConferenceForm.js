import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            maxPresentations: '',
            maxAttendees: '',
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleEndDate = this.handleEndDate.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentations = this.handleMaxPresentations.bind(this);
        this.handleMaxAttendees = this.handleMaxAttendees.bind(this);
        this.handleLocationName = this.handleLocationName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStartDate(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndDate(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentations(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }

    handleMaxAttendees(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }

    handleLocationName(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;

        console.log(data);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(conferenceUrl, fetchConfig);

        if(response.ok){
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                maxPresentations: '',
                maxAttendees: '',
                location: '',
            };

            console.log("cleared:", cleared);

            this.setState(cleared);

            console.log("state after submit:", this.state);
        }
    }

    async componentDidMount () {
        const url = "http://localhost:8000/api/locations/"

        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            console.log(data);
            this.setState({locations: data.locations})
        }
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit} id="create-conference-form">

                <div className="form-floating mb-3">
                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>

                <div className="form-floating mb-3">
                <input value={this.state.starts} onChange={this.handleStartDate} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
                </div>

                <div className="form-floating mb-3">
                <input value={this.state.ends} onChange={this.handleEndDate} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
                </div>

                <div className="form mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={this.state.description} onChange={this.handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" cols="30" rows="4"></textarea>
                </div>

                <div className="form-floating mb-3">
                <input value={this.state.maxPresentations} onChange={this.handleMaxPresentations} required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="max_presentations">Max Presentations</label>
                </div>

                <div className="form-floating mb-3">
                <input value={this.state.maxAttendees} onChange={this.handleMaxAttendees} required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="max_attendees">Max Attendees</label>
                </div>

                <div className="mb-3">
                <select value={this.state.location} onChange={this.handleLocationName} required id="location" name="location" className="form-select">
                <option value="">Choose Location</option>
                {this.state.locations.map(location => {
                    return (
                        <option key={location.name} value={location.id}>
                        {location.name}
                        </option>
                    );
                })}
            </select>

            </div>

            <button className="btn btn-primary">Create</button>

        </form>
        )
    }
}

export default ConferenceForm;
